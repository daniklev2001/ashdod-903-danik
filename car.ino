#include <SimpleTimer.h>
#include <Servo.h>
#include <Ultrasonic.h>



#define W 15

#define enA 5
#define enB 6

#define in1 2
#define in2 4
#define in3 7
#define in4 8


#define DELAY_LCD_COMMAND 20

#define BUZZER_PIN  13
#define RADAR_SERVO_PIN 9
#define ULRASONIC_PIN_1 11
#define ULRASONIC_PIN_2 12

//define radar variables 
#define RADAR_ENABLE true
#define RADAR_ROTATE_ENABLE false
#define RADAR_SOUND_ENABLE false

#define RADAR_ANGLE 35
#define RADAR_FULL_ANGLE_RANGE 180

#define RADAR_DELAY_MSEC 50
#define RADAR_ROTATE_DELAY_MSEC 30

// TODO  real value is 10 , For debug 1000  
#define MAX_CNT_UPDATE_DISTANCE  10  // RADAR_DELAY_MSEC * MAX_CNT_UPDATE_DISTANCE ~ 500msec

// radar_degree:  current state of radar init value is 0 half of full range
int radar_degree=RADAR_FULL_ANGLE_RANGE/2;

// radar_clock_direction:  current state of moving direction 
bool radar_clock_direction=true;

int cntUpdateDistance = 0;


#define DISTANCE_COLOR_DEFAULT  "46815"
#define DISTANCE_COLOR_LONG  "51161"
#define DISTANCE_COLOR_MEDIUM  "65458"
#define DISTANCE_COLOR_SHORT  "60793"



//all distance range in cm  
#define LONG_DISTANCE_RANGE 50
#define MEDIUM_DISTANCE_RANGE 25
#define SHORT_DISTANCE_RANGE 10
#define EXTREEM_DISTACE_RANGE 5

// all duration im msec
#define RADAR_BEEP_DURATION 200
#define SHORT_BEEP_INTERVAL 100
#define MEDIUM_BEEP_INTERVAL 300
#define LONG_BEEP_INTERVAL 500

SimpleTimer timerRadarRotate;
SimpleTimer timerRadarMeasur;
SimpleTimer timerRadarBeep;

Servo servoRadar;
Ultrasonic ultrasonicSensor(ULRASONIC_PIN_1, ULRASONIC_PIN_2);

boolean isRadarEnable = RADAR_ENABLE;
boolean isRadarRotating = RADAR_ROTATE_ENABLE;
boolean isRadarSound = RADAR_SOUND_ENABLE;

int sonarDistance=0;

int RMotorSpeed = 0;
int LMotorSpeed = 0;

int state;
String data;
String a,b,c;

int commaIndex,secondCommaIndex ;
int x = 0;
int y = 0;
int r = 0;
int v = 0;
int s = 0;
int r1 = 0;
int r2 = 0;
int s1 = 0;
int s2 = 0;

void setup()
{

  //TODO  addd remarks for A B  
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);

  //TODO  addd remarks for in1-4
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  //TODO add remark
  pinMode(BUZZER_PIN, OUTPUT);

  if ( isRadarEnable )
  {    
      // Init Servo to coresponding pin
      if ( !servoRadar.attached())
        servoRadar.attach(RADAR_SERVO_PIN); 
      
      timerRadarMeasur.setInterval(RADAR_DELAY_MSEC, radarMeasur);      
      
      setRadarRotating(isRadarRotating);      
  }    
  

 // nexInit();
 // nexSerial.begin(9600);

  Serial.begin(9600);
 // updatebaudRateLCD();
 // Serial.begin(57600 );
  
  Serial.println("Serial On"); //Print this messages when the serial port is connected
 
}


void loop()
{
  
  if (Serial.available()) {
    readSerialPort();
    //getBT();
   // drive();
  }
 
  
  /*if ( RADAR_ENABLE ){
    timerRadarRotate.run();
    timerRadarMeasur.run();
    timerRadarBeep.run();
  }*/
}

void readSerialPort(){
  int incoming_byte = 0; //Serial.read(); // read the incoming byte:
  byte data [7];
  String message = "";
  char read_char;

  incoming_byte = Serial.readBytesUntil (13,data,7);
  //Serial.print("I received: "+incoming_byte);  
  
  if ( incoming_byte >=7 ){
    parcerSerialData(data);
  }

  //TODO complete recieved all data   
}


void parcerSerialData(byte* data_port){
 // String s = char(data_port[0]);// +" "+ data_port[1] +" "+ data_port[2] +" "+ data_port[3] +" "+ data_port[4] +" "+ data_port[5] ;
  //sprintf(buffer, "%02x", number); 
  char s[14];
  sprintf(s, "%02x\n%02x\n%02x\n%02x", data_port[0], data_port[1], data_port[2], data_port[3]);
  Serial.println(s);
  if ( data_port[0] == 0x65 )
    if ( data_port[1] == 0x00)      
        if ( data_port[3] == 0x01 )
        {
          if (data_port[2] ==0x02){
            Serial.println("PLAY");
          }  
          else if (data_port[2] == 0x03){
            Serial.println("RESET");  
          }
          else if (data_port[2] == 0x04){
            changeRadarSound();
          }
          else if (data_port[2] == 0x05){
            Serial.println("BT");  
          }
          else if (data_port[2] == 0x06){
            changeRadarRotate();
          }
          else                          
            Serial.println("UNKNOW");
        }
        
}


/* 
 * set servo to center(init) position before use 
 */
void initRadarToCenter(){
  
   // set servo to home position before initial position
      servoRadar.write(0);

      // delay for  home position  moving
      delay(1000);
      
      //  init position for servo radar 90 degree
      servoRadar.write(radar_degree);

      // delay for  init position  moving
      delay(1000);      
}


/*
 *  Procedure set 
 */
void setRadarRotating(boolean newRadarState){

    // Init Servo to coresponding pin
    //if ( !servoRadar.attached())
    //  servoRadar.attach(RADAR_SERVO_PIN); 

    // set servo to center(init) position before use
    initRadarToCenter(); 
  
   if ( newRadarState ){                           
      // start timer for rotating
      timerRadarRotate.enable(0);
      timerRadarRotate.setInterval(RADAR_ROTATE_DELAY_MSEC, radarRotate);            
   }
   else{      
    timerRadarRotate.disable(0);
      //servoRadar.detach();
   }     
}


/*  This procedure rotate radar for predefine angle
 * run by external radar timer every predefine time ~ 10 msec
 */
void radarRotate()
{  
  servoRadar.write(radar_degree);
  if(radar_clock_direction)
  {
    radar_degree++;
    if(radar_degree >= (RADAR_FULL_ANGLE_RANGE/2)+RADAR_ANGLE)
    {
      radar_clock_direction = false;
    }
  }
  else
  {
    radar_degree--;
    if(radar_degree <= (RADAR_FULL_ANGLE_RANGE/2)-RADAR_ANGLE)
    {
      radar_clock_direction = true;
    }
  }  
}



/*  Procedure call back for timer: timerRadarBeep
 *  start beep with buzzer and start new timer for stop beeping after predefine time
 */
void beepRadarOn(){   
  //For debug only  print distance to serial port
  //Serial.println(sonarDistance);
   timerRadarBeep.setTimeout(RADAR_BEEP_DURATION,beepRadarOff);   
  digitalWrite(BUZZER_PIN, HIGH);

  //For update LCD now
  cntUpdateDistance = MAX_CNT_UPDATE_DISTANCE;
    
}

/*  Procedure call back for timer: timerRadarBeep
 *  stop beep with buzzer and disable timerRadarBeep
 */
void beepRadarOff(){   
  digitalWrite(BUZZER_PIN, LOW);        
}



void updatebaudRateLCD(){
  delay(1000);
  
  //Serial.begin(9600);
  Serial.print("bauds=57600");
  delay(DELAY_LCD_COMMAND);
  Serial.write(0xff);
  Serial.write(0xff);
  Serial.write(0xff);
  delay(1000);  
}

void updateLcdText(String txtControl, String stringValue){
   Serial.print(txtControl+".txt=");  // This is sent to the nextion display to set what object name (before the dot) and what atribute (after the dot) are you going to change.
  Serial.print("\"");  
  Serial.print(stringValue);  // This is the value you want to send to that object and atribute mentioned before.
  Serial.print("\"");  
  
  delay(DELAY_LCD_COMMAND);
  
  Serial.write(0xff);  // We always have to send this three lines after each command sent to the nextion display.
  Serial.write(0xff);
  Serial.write(0xff);
}

void updateLcdColor(String txtControl, String newColor){  
  Serial.print(txtControl+".bco="+newColor);  // This is sent to the nextion display to set what object name (before the dot) and what atribute (after the dot) are you going to change.     
  
  delay(DELAY_LCD_COMMAND);
  
  Serial.write(0xff);  // We always have to send this three lines after each command sent to the nextion display.
  Serial.write(0xff);
  Serial.write(0xff);
}





String prevLcdColorValue = "";

/*  This procedure check the distance from sonar and beep
 *  3 level alarm by predefine range distance
 *  if sound beep enable
 */
void radarMeasur(){

    //read data from ultrasonic in cm
    sonarDistance = ultrasonicSensor.read(CM);
    

    //check if value is not 0 ( may be error by inncorect reading )
    if (sonarDistance == 0 )
      return;
         
    String updateLcdColorValue = DISTANCE_COLOR_DEFAULT;
    //For debug only  print distance to serial port
    //Serial.println(sonarDistance);

    //Check the range of distance
    if ( sonarDistance <= LONG_DISTANCE_RANGE ) {        
      
      //check if beep alarm nut running now
      if ( timerRadarBeep.isEnabled(0) || timerRadarBeep.isEnabled(1) )
        return;
              
      int interval_beteen_beep;
      if(sonarDistance <= SHORT_DISTANCE_RANGE) 
      {
        interval_beteen_beep = SHORT_BEEP_INTERVAL;
        updateLcdColorValue = DISTANCE_COLOR_SHORT;
      }
      else if(sonarDistance <= MEDIUM_DISTANCE_RANGE) 
      {
        interval_beteen_beep = MEDIUM_BEEP_INTERVAL;
        updateLcdColorValue = DISTANCE_COLOR_MEDIUM;
      }
      else if(sonarDistance <= LONG_DISTANCE_RANGE) 
      {
        interval_beteen_beep = LONG_BEEP_INTERVAL;
        updateLcdColorValue = DISTANCE_COLOR_LONG;
      }
      
      //TODO  add EXTREEM_DISTACE_RANGE for prevent accident

      //launch beep timer if avaliable    
      if ( isRadarSound )
        timerRadarBeep.setTimeout(interval_beteen_beep,beepRadarOn);                
    } 

    //update LCD screen every 500 msec with distance and color coresponding to range
    cntUpdateDistance++;
    if ( cntUpdateDistance > MAX_CNT_UPDATE_DISTANCE ){
      cntUpdateDistance = 0;
      
      /*updateLcdText("edDistance",String(sonarDistance)+" cm");                  
      if ( prevLcdColorValue != updateLcdColorValue )  
        updateLcdColor("edDistance",updateLcdColorValue);   
      prevLcdColorValue = updateLcdColorValue;  */
    }
  
}

// Command from LCD interface to arduino module


/*
 * Restarts program from beginning but does not reset the peripherals and registers
 */
void resetArduinoCode(){ 
  asm volatile ("  jmp 0");  
}

/*
 * 
 */
void changeMotorDrive(){ 
}

/*
 * 
 */
void changeRadarRotate(){
  isRadarRotating = !isRadarRotating;
  setRadarRotating( isRadarRotating );  
}

/*
 * 
 */
void changeRadarSound(){  
  isRadarSound = !isRadarSound;
  if ( !isRadarSound){
    beepRadarOff();
    timerRadarBeep.disable(0);
  }
}

/*
 * 
 */
void getBluetoothStatus(){  
  /* try to send to bluetooth module at command 
   *  AT command : AT+ STATE?
   * Response: + STATE：<Param> OK
   * Parameter: “INITIALIZED”  “READY”  “PAIRABLE” “PAIRED” “INQUIRING” “CONNECTING” “CONNECTED” “DISCONNECTED” “NUKNOW” ???
   */
}


void getBT()
{
  data = Serial.read();
  Serial.println(data);
  /*commaIndex = data.indexOf('*');
  secondCommaIndex = data.indexOf('@');
  a = data.substring(1,commaIndex);
  b = data.substring(commaIndex+1,secondCommaIndex);
  x = a.toInt();
  y = b.toInt();
  v = abs(y); 
  r = abs(map(x, -350, 350, -100, 100));*/
}

void drive()
{
  if(y>0)
  {
    // Set Motor A backward
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
    // Set Motor B backward
    digitalWrite(in3, HIGH);
    digitalWrite(in4, LOW);
  }
  else if(y<0)
  {
    // Set Motor A forward
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
    // Set Motor B forward
    digitalWrite(in3, LOW);
    digitalWrite(in4, HIGH);
  }
  if(v)
  {
    RMotorSpeed = map(v, 1, 350, 70, 255);
    LMotorSpeed = map(v, 1, 350, 70, 255);
  }
  else
  {
    RMotorSpeed=0;
    LMotorSpeed=0;
  }
  if(x)
  {
    calcR();
  }
  Serial.print(LMotorSpeed);
  Serial.print(",");
  Serial.println(RMotorSpeed);
  analogWrite(enA, LMotorSpeed);
  analogWrite(enB, RMotorSpeed);
}

void calcR()
{
  r1 = r-W/2;
  r2 = r+W/2;
  if(y)
  {
    if(x>0)
    {//r10 v255
      s2 = v;
      s1 = v*r2/r1;
    }
    else if(x<0)
    {
      s1 = v;
      s2 = v*r1/r2;
    }
  }
  else
  {
    
  }
  LMotorSpeed = s1;
  RMotorSpeed = s2;
}
