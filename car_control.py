from bluetooth import *

x=0
y=0

x1=0
y1=0

client_socket=BluetoothSocket( RFCOMM )
client_socket.connect(("98:D3:71:F9:68:25", 1))

client_socket.send("connected#")


def printControl():
	if x==0:
		x1=0
	elif x==1:
		x1=100
	elif x==2:
		x1=150
	elif x==3:
		x1=200
	elif x==-1:
		x1=-90
	if y>0:
		y1 = 5
	elif y<0:
		y1 = -5
	else:
		y1 = 0
	data = ("~{}*{}@0#".format(y1, x1))
	print(data)
	client_socket.send(data)

while True:
	key = input()
	try:
		if key is 'w': 
			if x<3:
				x=x+1
			printControl()
		elif key is 'a':
			if x>-3:
				y=y-1
			printControl()
		elif key is 's':
			if x>-1:
				x=x-1
			printControl()
		elif key is 'd':
			if y<3:
				y=y+1
			printControl()
		elif key is ' ':
			x=0
			y=0
			printControl()
		elif key is 'q':
			client_socket.send("disconnected#")
			client_socket.close()
			break
	except:
		pass
