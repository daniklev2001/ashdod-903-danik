import serial.tools.list_ports as port_list
import matplotlib.animation as animation
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, lfiltic, sosfilt, sosfreqz
from collections import deque
from tkinter import *
import numpy as np
import threading
import keyboard
import serial
import queue
import math
import time
import sys
import os

#variables for signal proccesing
ploter = [deque(maxlen=480),deque(maxlen=480),deque(maxlen=480),deque(maxlen=480)]
filtered = [deque(maxlen=480),deque(maxlen=480),deque(maxlen=480),deque(maxlen=480)]
derivative = [deque(maxlen=480),deque(maxlen=480),deque(maxlen=480),deque(maxlen=480)]
rng = [[],[],[],[]]
bnd = [deque(maxlen=480),deque(maxlen=480),deque(maxlen=480),deque(maxlen=480)]
#variables for GUI
lines_tag = [[],[],[],[]]
W=800
H=416
MAX_VALUE=1024
dft_size = 128
spect = [0]*int(dft_size/10)
sp = int(H/len(spect))
spect_w = int(sp/3*2)
offset_a = int((H-sp*len(spect))/2)
offset_b = 30
pi = math.pi
	
#finding open serial by scaning diractory
def findSerial():
	for i in range(4):
		if(os.path.exists("/dev/ttyACM%d"%i)):
			return ("/dev/ttyACM%d"%i)

#connecting to emg module
path = "COM3"
print("Connecting to: " + path)
ser = serial.Serial(path, 115200)
print("Connected successfully to emg module")
print("Connection info: " + str(ser))

master = Tk()

w = Canvas(master, width=W, height=H, background='white')
w.pack()

def drawGrid():
	w.create_line(W/5*2, 0, W/5*2, H)
	w.create_line(W/5*2, H/4, W, H/4)
	w.create_line(W/5*2, H/4*2, W, H/4*2)
	w.create_line(W/5*2, H/4*3, W, H/4*3)
	for i in range(len(spect)):
		spect[i] = w.create_rectangle(0,offset_a+sp*i+offset_b,160,offset_a+sp*i-spect_w+offset_b)
drawGrid()

index = [320,320,320,320]
lp = [0,0,0,0]

def plotGraph(data,ch,maxV):
	#print(data)
	data = list(data)
	index[ch] = W/5*2
	#lp[ch] = 0
	for line in lines_tag[ch]:
		w.delete(line)
	lines_tag[ch] = []
	#master.update()
	for v in data:
		y=int(104-(104/maxV)*v)
		lines_tag[ch].append(w.create_line(index[ch],lp[ch]+ch*104,index[ch]+1,y+ch*104,width=2,fill='red'))
		index[ch] += 1
		lp[ch] = y
	#master.update()

def lineTo(v,ch,maxV):
	y=int(104-(104/maxV)*v)
	if index[ch] >= W: 
		index[ch] = W/5*2
	else:
		if len(lines_tag[ch]) > 480:
			w.delete(lines_tag[ch][0])
			lines_tag[ch].remove(lines_tag[ch][0])
		lines_tag[ch].append(w.create_line(index[ch],lp[ch]+ch*104,index[ch]+1,y+ch*104,width=2,fill='red'))
		master.update()
		index[ch] += 1
	lp[ch] = y
	
#50hz filter
#input: signal array
#output: filtered signal array
def fftyhzfilter(src):
	n = 5
	data = [0]*10
	findex = len(src)-10
	for v in range(10):
		for i in range(n):
			try:
				data[v] = data[v] + src[findex+v-i]
			except:
				pass
		data[v] = data[v] / n
	return data

#derevetive function
#input: signal array
#output: derevetive of signal array
def de(src):
	data = [0]*500
	for v in range(len(src)):
		try:
			data[v] = ((src[v+1]-src[v-1])/2)
		except:
			pass
	return data	
	
def ret_iir(b, a, input, output):
	x = list(input.queue)
	y = list(output.queue)
	for n in range(len(data)):
		v1 = 0
		for i in range(len(b)):
			v1 += (b[i] * x[n-i])
		v2 = 0
		for j in range(1,len(a)):
			v2 += (a[j] * y[n-j])
		y[n] = (v1-v2)/a[0]
		
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
	y = []
	try:
		sig = [data.get() for _ in range(dft_size+3)]
		b, a = butter_bandpass(lowcut, highcut, fs, order=order)
		zi = lfiltic(b, a, y=sig)
		y = lfilter(b, a, sig, zi=zi)
	except Exception as e:
		print(e)
	return y[0]

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a
	
def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y
	
def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='high', analog=False)
    return b, a

def butter_highpass_filter(data, cutoff, fs, order=5):
    b, a = butter_highpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

#Discrete Fourier Transform
#input: signal array
#output: array of frequancy strengths
def dft(x):
	N = dft_size
	N2 = int(N/2)
	x = list(x)[-N:]
	X = [0]*N2
	k = 5
	if(len(x) >= N):
		for k in range(N2):
			re = 0
			im = 0
			for n in range(N):
				phi = (2*pi*k*n)/N
				re += x[n] * math.cos(phi)
				im -= x[n] * math.sin(phi)
			re = re/N
			im = im/N
			freq = k
			amp = math.sqrt(re*re+im*im)
			X[k] = (freq, amp)
	return X

#Root mean square
#input: signal array
#output: array of rms`ed signal
def rms(x):
	N = len(x)
	d = 3
	X = [0]*(N-d)
	for n in range(N-d):
		v=0
		for i in range(d):
			v += math.pow(x[n+i],2)
		X[n] = (math.sqrt(v/d))
	return X

#shows strengths of frequancys
def plotSPEC(x):
	for i in range(int(dft_size/10)):
		w.coords(spect[i],0,offset_a+sp*i+offset_b,x[5*i][1]*0.5,offset_a+sp*i-spect_w+offset_b)

#variables for serial buffer
buf = [ queue.Queue() for i in range(4) ]
bnd_buf = [ queue.Queue() for i in range(4) ]

#reads serial from emg module
def readSerial():
	while True:
		try:
			line = ser.readline()
			string = line.decode('utf-8')[:len(line)-2]
			ch = int(string[1])
			if 0 <= ch <= 3:
				index,val,checksum=string[3:-1].split("#") #parsering string
				values = list(map(int,val.split(",")))#gets all values from string	
				if(getChecksum(values)==int(checksum)): #checks if checksum is correct
					ploter[ch].extend(values)
					#filtered[ch].extend(fftyhzfilter(ploter[ch]))
					filtered[ch].extend(ploter[ch])
					for v in list(filtered[ch])[-10:]:
						buf[ch].put(v)
						bnd_buf[ch].put(v)
		except Exception as e:
			print(e)

def proccess_sig():
	view = 1
	while True:
		start = time.time()
		for ch in range(4):
			values = rms(abs(butter_bandpass_filter(bnd_buf[ch],1,49,100)))			
			print("ch",ch,"buf size: " , bnd_buf[ch].qsize())
			rng[ch] = dft(values)
			print(ch , rng[ch])
		end = time.time()
		print("delay: " , (end - start))
		print(view)
		try:
			plotSPEC(rng[view])
		except:
			pass
		if keyboard.is_pressed('q'):
			quit()
		elif keyboard.is_pressed('1'):
			view = 0
		elif keyboard.is_pressed('2'):
			view = 1
		elif keyboard.is_pressed('3'):
			view = 2
		elif keyboard.is_pressed('4'):
			view = 3
		time.sleep(0.5)
		
def avrg(x, f, t):
	sum = 0
	N = 2*t+1
	for n in range(N):
		sum = sum + x[f-t+n][1]
	return (sum)/N
		
threshold = [(35,50),(35,50),(35,50),(35,600)]		

def detectMusc():
	while True:
		for ch in range(4):
			try:
				if avrg(rng[ch], (threshold[ch])[0], 5) > threshold[ch][1]:
					pass
					#controlCar(ch)
					#print("detected muscle ch:",ch)
			except Exception as e:
				pass
		time.sleep(0.5)
		
def printControl():
	if x==0:
		x1=0
	elif x==1:
		x1=100 
	elif x==2:
		x1=150
	elif x==3:
		x1=200
	elif x==-1:
		x1=-90
	if y>0:
		y1 = 5
	elif y<0:
		y1 = -5
	else:
		y1 = 0
	data = ("~{}*{}@0#".format(y1, x1))
	print(data)

def controlCar(ch):
	try:
		if ch is 1: 
			if xSpeed<3:
				xSpeed=xSpeed+1
		elif ch is 2:
			if xSpeed>-3:
				ySpeed=ySpeed-1
		elif ch is 3:
			if xSpeed>-1:
				xSpeed=xSpeed-1
		elif ch is 4:
			if ySpeed<3:
				ySpeed=ySpeed+1
		elif ch is 4 and ch is 2:
			xSpeed = 0
			ySpeed = 0
		printControl()
	except:
		pass
		
def updateGUI():
	while True:	
		for ch in range(4):
			q = buf[ch]
			try:
				leng = len(list(q.queue))
				if(leng):
					v = q.get()
					lineTo(v,ch,1024)
				else:
					time.sleep(0.00125)
			except Exception as e:
				print(e)

def getChecksum(values):
    check=0
    for i in range(10):
        check = check + ((i+1)*values[i])
    return check
	
t1 = threading.Thread(target=readSerial)
t2 = threading.Thread(target=updateGUI)
t3 = threading.Thread(target=proccess_sig)
t4 = threading.Thread(target=detectMusc)
t1.start()
t2.start()
t3.start()
t4.start()
mainloop()

def stop_prog():
	t1.stop()
	t2.stop()
	t3.stop()