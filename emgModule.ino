#include <SimpleTimer.h>
#define PCK_SIZE 10
#define REC_SIZE 10

typedef struct packet {
  int ch = 0;
  int data[PCK_SIZE] = { 0 };
  int index = 0;
  unsigned int checksum = 0;
  int counter = 0;
}classPacket;

void sendValues();
void add(classPacket* pck, int sig);
void updateChecksum(classPacket* pck);
char* sendPacket(classPacket* pck);
void sendRecovery(int index);
void test(classPacket* pck);

const int analogInPin0 = A0;
const int analogInPin1 = A1;
const int analogInPin2 = A2;
const int analogInPin3 = A3;

int sensor1Value = 0;
int sensor2Value = 0;
int sensor3Value = 0;
int sensor4Value = 0;

String ser = "";
String data = "";

SimpleTimer t;

String rec[REC_SIZE];
int ind[REC_SIZE];
int counter = 0;

classPacket pck1;
classPacket pck2;
classPacket pck3;
classPacket pck4;

void setup() {
  Serial.begin(115200);
  t.setInterval(5, sendValues);
}

void loop() {
  t.run();
}

void sendValues()
{
  sensor1Value = analogRead(analogInPin0);
  add(&pck1, sensor1Value, 0);
  sensor2Value = analogRead(analogInPin1);
  add(&pck2, sensor2Value, 1);
  sensor3Value = analogRead(analogInPin2);
  add(&pck3, sensor3Value, 2);
  sensor4Value = analogRead(analogInPin3);
  add(&pck4, sensor4Value, 3);
  if (Serial.available()) {
    if (ser[0] == 'R')
    {
      //sendRecovery();
    }
  }
}

void add(classPacket* pck, int sig, int ch)
{
  pck->ch = ch;
  pck->data[pck->index] = sig;
  pck->index++;
  if (pck->index == PCK_SIZE)
  {
    sendPacket(pck);
    pck->index = 0;
    pck->counter++;
  }
}

void updateChecksum(classPacket* pck) {
  int i, result = 0;
  for (i = 0; i < PCK_SIZE; i++)
  {
    result += ((i + 1) * pck->data[i]);
  }
  pck->checksum = result;
}

char* sendPacket(classPacket* pck)
{
  data = "";
  int i;
  updateChecksum(pck);

  data += ("D");
  data += (pck->ch);
  data += ("{");
  data += (pck->counter);
  data += ("#");
  for (i = 0; i < PCK_SIZE - 1; i++) {
    data += (pck->data[i]);
    data += (",");
  }
  data += (pck->data[PCK_SIZE - 1]);
  data += ("#");
  data += (pck->checksum);
  data += ("}");
  data += ("");
  Serial.println(data);
  rec[counter] = data;
  ind[counter] = pck->counter;
  counter++;
  if (counter == REC_SIZE)
  {
    counter = 0;
  }
}

void sendRecovery(int index)
{
  for (int i = 0; i < REC_SIZE; i++)
  {
    if (ind[i] == index)
    {
      Serial.println(rec[i]);
    }
  }
}
